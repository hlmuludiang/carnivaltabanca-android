package com.muludiang.carnivaltabanca;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.muludiang.carnivaltabanca.adapter.VideoAdapter;
import com.muludiang.carnivaltabanca.utilties.Videos;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;

import java.util.List;

public class MusicFragment extends Fragment {

    private static final String TAG = "Video fragment";
    private ProgressDialog mProgressDialog;
    private VideoAdapter videoAdapter;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.videos_layout, container, false);

        videoAdapter = new VideoAdapter(getActivity());
        videoAdapter.setPaginationEnabled(false);

        listView = (ListView) rootView.findViewById(R.id.video_list);

        videoAdapter.addOnQueryLoadListener(new OnQueryLoadListener<Videos>() {

            @Override
            public void onLoaded(List<Videos> objects, Exception e) {
                // TODO Auto-generated method stub
                mProgressDialog.dismiss();
            }

            @Override
            public void onLoading() {
                // TODO Auto-generated method stub
                // Create processDialog
                mProgressDialog = new ProgressDialog(getActivity());
                // Set title
                mProgressDialog.setTitle("Music Videos");
                // Progressdialog message
                mProgressDialog.setMessage("Loading, please wait...");
                // Show progressDialog
                mProgressDialog.show();
            }
        });
        listView.setAdapter(videoAdapter);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.v(TAG, "Destroyed?");


    }
}
