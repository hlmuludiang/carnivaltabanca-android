// Source:  http://www.vogella.com/tutorials/AndroidGoogleMaps/article.html

package com.muludiang.carnivaltabanca;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.muludiang.carnivaltabanca.utilties.ParseProxyObject;
import com.parse.ParseObject;

import java.io.IOException;
import java.util.List;

public class CarnivalDetails extends Activity implements OnMapReadyCallback {
    static final LatLng HAMBURG = new LatLng(53.558, 9.927);
    static final LatLng KIEL = new LatLng(53.551, 9.993);
    ParseObject carivalObject;
    // private GoogleMap map;
    Context context;
    private String streetAddress = "";
    private ParseProxyObject ppo;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.carnival_detail_layout);

        Intent intent = getIntent();
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color
                .parseColor("#D83A3A")));
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(intent.getStringExtra("carnivalName"));
        actionBar.setSubtitle(intent.getStringExtra("carnivalDates"));
        actionBar.setDisplayHomeAsUpEnabled(true);

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ppo = (ParseProxyObject) intent.getSerializableExtra("carnivalObject");

        if (ppo.getString("State") == "") {
            streetAddress = ppo.getString("City") + ", "
                    + ppo.getString("Country");
        } else {
            streetAddress = ppo.getString("City") + ", "
                    + ppo.getString("State") + ", " + ppo.getString("Country");
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.carnival_details_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Called when the Home (Up) button is pressed
                Intent upIntent = new Intent(this, MainActivity.class);
                upIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(upIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setParseObject(ParseObject object) {
        this.carivalObject = object;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        // TODO Auto-generated method stub
        Geocoder gc = new Geocoder(this);
        List<Address> addressList = null;
        try {
            addressList = gc.getFromLocationName(streetAddress, 1);
        } catch (IOException e) {

        } finally {
            Address address = addressList.get(0);
            if (address.hasLatitude() && address.hasLongitude()) {
                double lat = address.getLatitude();
                double lng = address.getLongitude();
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        lat, lng), 13));

                map.addMarker(new MarkerOptions().title(ppo.getString("City"))
                        .position(new LatLng(lat, lng)));
            }
        }
    }
}
