package com.muludiang.carnivaltabanca;

import com.muludiang.carnivaltabanca.utilties.MyUtilies;
import com.parse.ParseObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CarnivalsListData {

    MyUtilies myUtils;
    private String name;
    private String location;
    private String voteCount;
    private Float ratings;
    private String imageName;
    private ParseObject carnivalObject;
    private String month;
    private String day;
    private String carnivalDates;

    public void setDates(Date startDate, Date endDate) {
        String[] dateFormats = new String[]{
                "MMM",
                "d",
                "EEE., MMM dd yyyy",
        };
        java.text.SimpleDateFormat monthDateFormat = new java.text.SimpleDateFormat(dateFormats[0], Locale.getDefault());
        java.text.SimpleDateFormat dayDateFormat = new java.text.SimpleDateFormat(dateFormats[1], Locale.getDefault());
        java.text.SimpleDateFormat carnivalDate = new SimpleDateFormat(dateFormats[2], Locale.getDefault());
        if (endDate == null) {
            this.carnivalDates = carnivalDate.format(startDate) + " - " + carnivalDate.format(startDate);
        } else {
            this.carnivalDates = carnivalDate.format(startDate) + " - " + carnivalDate.format(endDate);
        }
        this.month = monthDateFormat.format(startDate);
        this.day = dayDateFormat.format(startDate);
    }

    public String getCarnivalDates() {
        return carnivalDates;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }

    public ParseObject getCarnivalObject() {
        return carnivalObject;
    }

    public void setCarnivalObject(ParseObject object) {
        this.carnivalObject = object;
    }

    public String getImage() {
        return imageName;
    }

    public void setImage(String country) {
        this.imageName = "flag_of_" + name.replace(" ", "-").toLowerCase(Locale.getDefault());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVotes() {
        return voteCount;
    }

    public void setVotes(Integer voters) {
        if (voters == null) {
            this.voteCount = "(0 votes)";
        } else if (voters == 1) {
            this.voteCount = "(" + voters + " vote)";
        } else {
            this.voteCount = "(" + voters + " votes)";
        }
    }

    public void setLocation(String city, String state, String country) {
        if (state == null) {
            this.location = city + " " + country;
        } else {
            this.location = city + " " + state;
        }

        this.imageName = "flag_of_" + country.replace(" ", "_").toLowerCase(Locale.getDefault());
        //Log.d("CarnivalsListData - filename:", this.imageName + "");
    }

    public String getLocation() {
        return location;
    }

    public void setRatings(Integer votes, Integer voters) {
        Integer Votes;
        Integer Voters;
        if (votes == null) {
            Votes = 0;
        } else {
            Votes = votes;
        }

        if (voters == null) {
            Voters = 0;
        } else {
            Voters = voters;
        }
        //Log.d("CarnivalsListData: votes", "" + Votes);
        //Log.d("CarnivalListData: voters", "" + Voters);
        if (Voters == 0) {
            this.ratings = 0f;
        } else {
            this.ratings = (float) ((Votes / Voters) * 5);
            //Log.d("CarnivalsListData: votes | voters | rating", Votes + " | " + Voters + " | " + this.ratings);
        }
    }

    public Float getRatings() {
        return ratings;
    }
}
