// Source: http://www.androidbegin.com/tutorial/android-parse-com-listview-images-and-texts-tutorial/

package com.muludiang.carnivaltabanca;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.muludiang.carnivaltabanca.adapter.CarnivalsCustomAdapter;
import com.muludiang.carnivaltabanca.adapter.VideoAdapter;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

public class MainActivity extends FragmentActivity {

    final static String TAG = "Main Activity";
    private final CarnivalsFragment carnivalsFragment = new CarnivalsFragment();
    private final MusicFragment musicFragment = new MusicFragment();
    private final ProfileFragment profileFragment = new ProfileFragment();
    private CarnivalsCustomAdapter carnivalsAdapter;
    private VideoAdapter videoAdapter;
    private ListView listView;
    private Integer selectedMenu;
    // Tab titles
    private StartAppAd startAppAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        StartAppSDK.init(this, "109374805", "212163324", true);
        startAppAd = new StartAppAd(this);

        Log.i(TAG, "Starting app");
        /*carnivalsAdapter = new CarnivalsCustomAdapter(this);
        carnivalsAdapter.setPaginationEnabled(false);
		videoAdapter = new VideoAdapter(this);
		videoAdapter.setPaginationEnabled(false);

		listView = (ListView) findViewById(R.id.main_list_layout);
		listView.setAdapter(carnivalsAdapter);
		carnivalsAdapter.loadObjects();*/

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fmtrans = fm.beginTransaction();
        fmtrans.add(R.id.main_layout, carnivalsFragment);
        fmtrans.commit();
        selectedMenu = R.id.carnival_menu_title;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {

            case R.id.carnival_menu_title: {
            /*if (listView.getAdapter() != carnivalsAdapter) {
                listView.setAdapter(carnivalsAdapter);
				carnivalsAdapter.loadObjects();
			} else {
				carnivalsAdapter.loadObjects();
			}*/
                //newFragment = new CarnivalsFragment();
                FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                trans.replace(R.id.main_layout, carnivalsFragment);
                trans.addToBackStack(null);
                trans.commit();
                break;
            }
            case R.id.music_menu_title: {
			/*if (listView.getAdapter() != videoAdapter) {
				listView.setAdapter(videoAdapter);

				videoAdapter.loadObjects();
			} else {
				videoAdapter.loadObjects();
			}*/
                FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                trans.replace(R.id.main_layout, musicFragment);
                trans.addToBackStack(null);
                trans.commit();
                Log.v(TAG, "**** Video button clicked ****");
                break;
            }
            case R.id.profile_menu_title: {

                FragmentTransaction trans = getSupportFragmentManager().beginTransaction();
                trans.replace(R.id.main_layout, profileFragment);
                trans.addToBackStack(null);
                trans.commit();
                break;
            }
            case R.id.action_settings: {
                break;
            }
        }
		
		/*
		 * int id = item.getItemId(); if (id == R.id.carnival_menu_title) { if
		 * (listView.getAdapter() != carnivalsAdapter) {
		 * listView.setAdapter(carnivalsAdapter);
		 * carnivalsAdapter.loadObjects(); } else {
		 * carnivalsAdapter.loadObjects(); } } else if (id ==
		 * R.id.music_menu_title) { if (listView.getAdapter() != videoAdapter) {
		 * listView.setAdapter(videoAdapter);
		 * 
		 * videoAdapter.loadObjects(); } else { videoAdapter.loadObjects(); } }
		 * else if (id == R.id.profile_menu_title) {
		 * 
		 * } else if (id == R.id.action_settings) { return true; }
		 */
        return super.onOptionsItemSelected(item);
    }

}
