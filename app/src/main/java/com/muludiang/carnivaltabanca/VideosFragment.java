package com.muludiang.carnivaltabanca;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.muludiang.carnivaltabanca.adapter.VideoAdapter;
import com.muludiang.carnivaltabanca.adapter.VideosListViewAdapter;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class VideosFragment extends Fragment {

    ListView videosListView;
    List<ParseObject> videoObject;
    ProgressDialog mProgressDialog;
    VideosListViewAdapter videosListViewAdpater;
    VideoAdapter videoAdapter;
    Context context;
    private List<VideosListData> videosData = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setContentView(R.layout.videos_layout);

        View rootView = inflater.inflate(R.layout.videos_layout, container,
                false);

        // new GetRemoteData().execute();
        /*videoAdapter = new VideoAdapter(getActivity());

		videosListView = (ListView) getActivity().findViewById(R.id.video_list);
		Log.i("VideoFragment", "***** Instance created ****");
		videosListView.setAdapter(videoAdapter);
		videoAdapter.setPaginationEnabled(false);
		videoAdapter.loadObjects();*/
        return rootView;
    }

    private class GetRemoteData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            // Create processDialog
            mProgressDialog = new ProgressDialog(getActivity());
            // Set title
            mProgressDialog.setTitle("Soca Videos");
            // Progressdialog message
            mProgressDialog.setMessage("Loading...");
            // Show progressDialog
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            videosData = new ArrayList<VideosListData>();
            try {
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(
                        "Videos");
                query.orderByAscending("createdAt");
                videoObject = query.find();
                for (ParseObject video : videoObject) {
                    VideosListData aVideo = new VideosListData();
                    aVideo.setVideo((String) video.get("videoId"));
                    aVideo.setRatings((Integer) video.get("votes"),
                            (Integer) video.get("voters"));

                    videosData.add(aVideo);
                }
            } catch (ParseException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            videosListView = (ListView) getActivity().findViewById(
                    R.id.video_list);

            // videosListViewAdpater = new VideosListViewAdapter(getActivity(),
            // videosData);
            // videosListView.setAdapter(videosListViewAdpater);
            mProgressDialog.dismiss();
        }
    }
}