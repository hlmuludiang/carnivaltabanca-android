package com.muludiang.carnivaltabanca.utilties;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Locale;

@ParseClassName("Carnivals")
public class Carnivals extends ParseObject {
    final static String TAG = "Carnivals.java";
    private String[] dateFormats = new String[]{
            "MMM",
            "d",
            "EEE., MMM dd yyyy",
    };
    private java.text.SimpleDateFormat monthDateFormat = new java.text.SimpleDateFormat(dateFormats[0], Locale.getDefault());
    private java.text.SimpleDateFormat dayDateFormat = new java.text.SimpleDateFormat(dateFormats[1], Locale.getDefault());
    private java.text.SimpleDateFormat carnivalDate = new SimpleDateFormat(dateFormats[2], Locale.getDefault());

    public Carnivals() {

    }

    public static ParseQuery<Carnivals> getQuery() {
        return ParseQuery.getQuery(Carnivals.class);
    }

    public String getCarnivalName() {
        return getString("Name");
    }

    public Float getRatings() {
        if (getInt("voters") == 0) {
            return 0f;
        } else {
            return (float) (getInt("votes") / getInt("voters")) * 5;
        }
    }

    public Integer getVoters() {
        return getInt("voters");
    }

    public String getLocation() {
        if (getString("State") == null) {
            return (getString("City") + " " + getString("Country"));
        } else {
            return (getString("City") + " " + getString("State"));
        }
    }

    public String getImageName() {
        return "flag_of_" + getString("Country").replace(" ", "_").toLowerCase(Locale.getDefault());
    }

    public String getMonth() {
        return monthDateFormat.format(getDate("Start"));
    }

    public String getDay() {
        return dayDateFormat.format(getDate("Start"));
    }

    public String getCarnivalDates() {
        // TODO Auto-generated method stub
        if (getDate("End") == null) {
            return carnivalDate.format(getDate("Start")) + " - " + carnivalDate.format(getDate("Start"));
        } else {
            return carnivalDate.format(getDate("Start")) + " - " + carnivalDate.format(getDate("End"));
        }
    }

    public ParseObject getCarnivalObject() {
        return this;
    }
}
