package com.muludiang.carnivaltabanca.utilties;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.muludiang.carnivaltabanca.Applications;
import com.muludiang.carnivaltabanca.R;

public class VideoPlayer extends YouTubeFailureRecoveryActivity {

    private static final String TAG = "Video Player class";
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private YouTubePlayerView playerView;
    private YouTubePlayer player;
    private String videoId;

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        super.onInitializationFailure(provider, youTubeInitializationResult);
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(getString(R.string.error_player), youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        super.onInitializationSuccess(provider, youTubePlayer, b);
        this.player = youTubePlayer;
        player.addFullscreenControlFlag(1);
        player.setFullscreen(true);
        if (!b) {
            player.cueVideo(videoId);
        }
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.video_player);
        playerView = (YouTubePlayerView) findViewById(R.id.playerView);
        playerView.initialize(Applications.YOUTUBE_DEVELOPER_KEY, this);
        Intent intent = getIntent();
        videoId = intent.getStringExtra("videoId");
        Log.v(TAG, "What just happened?");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player = null;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }
}
