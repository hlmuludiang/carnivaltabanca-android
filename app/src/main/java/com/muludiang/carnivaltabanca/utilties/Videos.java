package com.muludiang.carnivaltabanca.utilties;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Videos")
public class Videos extends ParseObject {
    public Videos() {

    }

    public String getVideoId() {
        return getString("videoId");
    }

    public Float getRating() {
        if (getInt("voters") == 0) {
            return 0f;
        } else {
            return (float) (getInt("votes") / getInt("voters")) * 5;
        }
    }

    public Integer getVoters() {
        return getInt("voters");
    }
}
