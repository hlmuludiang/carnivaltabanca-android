package com.muludiang.carnivaltabanca;

import android.app.Application;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.flurry.android.FlurryAgent;
import com.muludiang.carnivaltabanca.utilties.Carnivals;
import com.muludiang.carnivaltabanca.utilties.Videos;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.startapp.android.publish.StartAppSDK;

import io.fabric.sdk.android.Fabric;
// http://www.androidbegin.com/tutorial/android-parse-com-listview-images-and-texts-tutorial/

public class Applications extends Application {

    public static final String YOUTUBE_DEVELOPER_KEY = "AIzaSyCGaTszIpguUZE2h9IDOnkOh5y6asbI2IE";
    final static String TAG = "Application APIs";

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        //Crashlytics.start(this);


        StartAppSDK.init(this, "109374805", "212163324", true);
        Parse.initialize(this, "AUZCfbqQmGURqmXzgY6rsOzycGfFkeIYKtY1M44M",
                "NWFmqCmylutmgLfBNhj9RG4EWDHwKhqj8pQC9ZDD");
        ParseObject.registerSubclass(Carnivals.class);
        ParseObject.registerSubclass(Videos.class);
        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });

        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, "K27YVK2FMNY3SHJ4PD22");
        Log.i(TAG, "Application APIs loaded!");


    }
}
