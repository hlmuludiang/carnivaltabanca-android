package com.muludiang.carnivaltabanca;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.muludiang.carnivaltabanca.adapter.CarnivalsCustomAdapter;
import com.muludiang.carnivaltabanca.adapter.CarnivalsListViewAdapter;
import com.muludiang.carnivaltabanca.utilties.Carnivals;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter.OnQueryLoadListener;

import java.util.List;

public class CarnivalsFragment extends Fragment implements OnItemClickListener {

    private static final String TAG = "Carnival fragments";
    List<ParseObject> parseObject;
    CarnivalsListViewAdapter carnivalsListViewAdapter;
    private ProgressDialog mProgressDialog;
    private CarnivalsCustomAdapter carnivalsAdapter;
    private List<CarnivalsListData> carnivalsData = null;
    private ListView listView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.carnival_layout, container,
                false);
        //listView = (ListView) rootView.findViewById(R.id.carnival_list);

        Log.i(TAG, "Starting app");

        carnivalsAdapter = new CarnivalsCustomAdapter(getActivity());
        carnivalsAdapter.setPaginationEnabled(false);

        listView = (ListView) rootView.findViewById(R.id.carnival_list);

        carnivalsAdapter.addOnQueryLoadListener(new OnQueryLoadListener<Carnivals>() {

            @Override
            public void onLoaded(List<Carnivals> objects, Exception e) {
                // TODO Auto-generated method stub
                mProgressDialog.dismiss();
            }

            @Override
            public void onLoading() {
                // TODO Auto-generated method stub
                mProgressDialog = new ProgressDialog(getActivity());
                // Set title
                mProgressDialog.setTitle("Carnival dates");
                // Progressdialog message
                mProgressDialog.setMessage("Loading, please wait...");
                // Show progressDialog
                mProgressDialog.show();
            }
        });
        //carnivalsAdapter.loadObjects();
        listView.setAdapter(carnivalsAdapter);
        return rootView;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

    }
}
