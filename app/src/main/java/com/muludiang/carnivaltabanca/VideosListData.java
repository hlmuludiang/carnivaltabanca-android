package com.muludiang.carnivaltabanca;

public class VideosListData {

    public String videoId;
    public String voteCount;
    public Float ratings;

    public String getVideo() {
        return videoId;
    }

    public void setVideo(String videoId) {
        this.videoId = videoId;
    }

    public String getVotes() {
        return voteCount;
    }

    public void setRatings(Integer votes, Integer voters) {
        Integer Votes;
        Integer Voters;
        if (votes == null) {
            Votes = 0;
        } else {
            Votes = votes;
        }

        if (voters == null) {
            Voters = 0;
        } else {
            Voters = voters;
        }

        if (Voters == 1) {
            this.voteCount = "(" + Voters + " vote";
        } else {
            this.voteCount = "(" + Voters + " votes";
        }

        if (Voters == 0) {
            this.ratings = 0f;
        } else {
            this.ratings = (float) ((Votes / Voters) * 5);
        }
    }

    public Float getRatings() {
        return ratings;
    }

}
