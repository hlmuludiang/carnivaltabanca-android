package com.muludiang.carnivaltabanca.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.muludiang.carnivaltabanca.CarnivalDetails;
import com.muludiang.carnivaltabanca.CarnivalsListData;
import com.muludiang.carnivaltabanca.R;
import com.muludiang.carnivaltabanca.utilties.ParseProxyObject;

import java.util.ArrayList;
import java.util.List;

public class CarnivalsListViewAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    private List<CarnivalsListData> carnivalsList = null;
    private ArrayList<CarnivalsListData> carnivalsArray;

    public CarnivalsListViewAdapter(Context context,
                                    List<CarnivalsListData> carnivalsList) {
        this.context = context;
        this.carnivalsList = carnivalsList;
        inflater = LayoutInflater.from(context);
        this.carnivalsArray = new ArrayList<CarnivalsListData>();
        this.carnivalsArray.addAll(carnivalsList);
    }

    public void myContext(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return carnivalsList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return carnivalsList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.carnival_listview_item, parent,
                    false);
            holder.location = (TextView) view
                    .findViewById(R.id.carnivalLocation);
            holder.name = (TextView) view.findViewById(R.id.carnivalName);
            holder.votes = (TextView) view.findViewById(R.id.carnival_votes);
            holder.ratings = (RatingBar) view.findViewById(R.id.carnival_ratings);
            holder.countryFlag = (ImageView) view
                    .findViewById(R.id.countryFlag);
            holder.month = (TextView) view.findViewById(R.id.carnivalMonth);
            holder.day = (TextView) view.findViewById(R.id.carnivalDay);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Set data to to view
        holder.name.setText(carnivalsList.get(position).getName());
        holder.location.setText(carnivalsList.get(position).getLocation());
        holder.votes.setText(carnivalsList.get(position).getVotes());
        holder.ratings.setRating(carnivalsList.get(position).getRatings());
        // holder.countryFlag.setImageResource(R.drawable.flag_of_anguilla);
        int ResId = context.getResources().getIdentifier(
                carnivalsList.get(position).getImage(), "drawable",
                context.getPackageName());
        holder.countryFlag.setImageResource(ResId);
        holder.month.setText(carnivalsList.get(position).getMonth());
        holder.day.setText(carnivalsList.get(position).getDay());

        // Listen for listview item click
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(context, CarnivalDetails.class);
                intent.putExtra("carnivalDates", carnivalsList.get(position).getCarnivalDates());
                intent.putExtra("carnivalName", carnivalsList.get(position).getName());
                intent.putExtra("carnivalObject", new ParseProxyObject(carnivalsList.get(position).getCarnivalObject()));
                context.startActivity(intent);
            }
        });
        return view;
    }

    public class ViewHolder {
        TextView name;
        TextView location;
        TextView votes;
        RatingBar ratings;
        ImageView countryFlag;
        TextView month;
        TextView day;
    }
}
