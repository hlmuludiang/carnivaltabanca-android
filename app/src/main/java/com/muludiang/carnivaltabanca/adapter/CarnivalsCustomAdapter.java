package com.muludiang.carnivaltabanca.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.muludiang.carnivaltabanca.CarnivalDetails;
import com.muludiang.carnivaltabanca.R;
import com.muludiang.carnivaltabanca.utilties.Carnivals;
import com.muludiang.carnivaltabanca.utilties.ParseProxyObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

public class CarnivalsCustomAdapter extends ParseQueryAdapter<Carnivals> {
    private Context context;

    public CarnivalsCustomAdapter(Context context) {

        super(context, new ParseQueryAdapter.QueryFactory<Carnivals>() {
            public ParseQuery<Carnivals> create() {
                ParseQuery<Carnivals> query = new ParseQuery<Carnivals>(
                        "Carnivals");
                query.orderByAscending("Start");
                Log.i("CarnivalsCustomAdapter", "Instance created");

                return query;
            }

        });
        // TODO Auto-generated constructor stub
        this.context = context;
    }

    @Override
    public View getItemView(final Carnivals carnival, View view, ViewGroup parent) {
        if (view == null) {
            view = View.inflate(getContext(), R.layout.carnival_listview_item,
                    null);
        }
        super.getItemView(carnival, view, parent);

        TextView carnivalName = (TextView) view.findViewById(R.id.carnivalName);
        carnivalName.setText(carnival.getCarnivalName());

        TextView voters = (TextView) view.findViewById(R.id.carnival_votes);
        if (carnival.getVoters() == 1) {
            voters.setText("( 1 vote )");
        } else {
            voters.setText("( " + carnival.getVoters() + " votes )");
        }

        RatingBar ratings = (RatingBar) view.findViewById(R.id.carnival_ratings);
        ratings.setRating(carnival.getRatings());

        int ResId = context.getResources().getIdentifier(
                carnival.getImageName(), "drawable",
                context.getPackageName());
        ImageView flag = (ImageView) view.findViewById(R.id.countryFlag);
        flag.setImageResource(ResId);

        TextView carnivalMonth = (TextView) view.findViewById(R.id.carnivalMonth);
        carnivalMonth.setText(carnival.getMonth());

        TextView carnivalDay = (TextView) view.findViewById(R.id.carnivalDay);
        carnivalDay.setText(carnival.getDay());

        TextView carnivalLocation = (TextView) view.findViewById(R.id.carnivalLocation);
        carnivalLocation.setText(carnival.getLocation());

        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(context, CarnivalDetails.class);
                intent.putExtra("carnivalDates", carnival.getCarnivalDates());
                intent.putExtra("carnivalName", carnival.getCarnivalName());
                intent.putExtra("carnivalObject", new ParseProxyObject(carnival.getCarnivalObject()));
                context.startActivity(intent);
            }
        });

        return view;

    }
}
