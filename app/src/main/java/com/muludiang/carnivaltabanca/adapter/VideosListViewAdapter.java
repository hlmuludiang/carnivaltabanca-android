package com.muludiang.carnivaltabanca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubePlayerFragment;
import com.muludiang.carnivaltabanca.R;
import com.muludiang.carnivaltabanca.VideosListData;

import java.util.ArrayList;
import java.util.List;

public class VideosListViewAdapter extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    private List<VideosListData> videosList = null;
    private ArrayList<VideosListData> videosArray;

    public VideosListViewAdapter(Context context,
                                 List<VideosListData> videosList) {
        this.context = context;
        this.videosList = videosList;
        inflater = LayoutInflater.from(context);
        this.videosArray = new ArrayList<VideosListData>();
        this.videosArray.addAll(videosList);
    }

    public void myContext(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return videosList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return videosList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.video_listview_item, parent,
                    false);
            holder.votes = (TextView) view.findViewById(R.id.video_votes);
            holder.ratings = (RatingBar) view.findViewById(R.id.video_ratings);
            //holder.youtubeVideo =  (YouTubePlayerFragment) getFragmentManager().findViewById(R.id.youtube_video);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Set data to to view

        holder.votes.setText(videosList.get(position).getVotes());
        holder.ratings.setRating(videosList.get(position).getRatings());
        //holder.youtubeVideo.set


        // Listen for listview item click
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
            }
        });
        return view;
    }

    public class ViewHolder {
        YouTubePlayerFragment youtubeVideo;
        TextView votes;
        RatingBar ratings;
    }

}
