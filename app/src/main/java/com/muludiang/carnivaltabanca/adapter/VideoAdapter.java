package com.muludiang.carnivaltabanca.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.muludiang.carnivaltabanca.R;
import com.muludiang.carnivaltabanca.utilties.VideoPlayer;
import com.muludiang.carnivaltabanca.utilties.Videos;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.io.InputStream;

public class VideoAdapter extends ParseQueryAdapter<Videos> {

    private final String TAG = "VideoAdapter";
    public Context context;
    private YouTubeThumbnailLoader myYouTubeThumbnailLoader;
    private YouTubeThumbnailView myYouTubeThumbnailView;

    public VideoAdapter(Context context) {
        super(context, new ParseQueryAdapter.QueryFactory<Videos>() {
            public ParseQuery<Videos> create() {
                ParseQuery<Videos> query = new ParseQuery<Videos>("Videos");
                query.orderByDescending("createdAt");
                query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
                Log.i("VideoAdapter", "Instance created");
                return query;
            }
        });

        this.context = context;
    }

    @Override
    public View getItemView(final Videos video, View view, ViewGroup parent) {
        if (view == null) {
            view = View.inflate(getContext(), R.layout.video_listview_item, null);
        }
        super.getItemView(video, view, parent);
        TextView votes = (TextView) view.findViewById(R.id.video_votes);
        if (video.getVoters() == 1) {
            votes.setText("( 1 vote )");
        } else {
            votes.setText("( " + video.getVoters() + " votes )");

        }

        RatingBar ratings = (RatingBar) view.findViewById(R.id.video_ratings);
        ratings.setRating(video.getRating());
        Log.v(TAG, "Setting video :" + video.getVideoId());

        // http://img.youtube.com/vi/<insert-youtube-video-id-here>/
        String videoURL = "http://img.youtube.com/vi/" + video.getVideoId() + "/default.jpg";
        ImageView videoImage = (ImageView) view.findViewById(R.id.videoThumb);
        new VideoThumbnailDownload(videoImage).execute(video.getVideoId());

        /* myYouTubeThumbnailView = (YouTubeThumbnailView) view.findViewById(R.id.video_thumb);

        Log.v(TAG, "Got video :" + video.getVideoId());
        myYouTubeThumbnailView.initialize(Applications.YOUTUBE_DEVELOPER_KEY, new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {

                myYouTubeThumbnailLoader = youTubeThumbnailLoader;
                myYouTubeThumbnailView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Log.v(TAG, "Clicked on video: " + video.getVideoId());
                        Intent intent = new Intent(context, VideoPlayer.class);
                        intent.putExtra("videoId", video.getVideoId());
                        context.startActivity(intent);
                    }
                });
                if (video.getVideoId() != null) {
                    try {
                        myYouTubeThumbnailLoader.setVideo(video.getVideoId());
                    } catch (Exception e) {
                        Log.e(TAG, "***** Video ID was null ");
                    }
                }
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                Log.v(TAG, "Error in init: " + youTubeInitializationResult.toString());
            }
        });*/


        return view;
    }

    private class VideoThumbnailDownload extends AsyncTask<String, Void, Bitmap> {
        ImageView videoImage;
        String videoURL;
        String videoId;

        public VideoThumbnailDownload(ImageView videoImage) {
            this.videoImage = videoImage;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            //String url = params[0];
            videoURL = "http://img.youtube.com/vi/" + params[0] + "/maxresdefault.jpg";
            videoId = params[0];
            Bitmap videoIcon = null;
            try {
                InputStream in = new java.net.URL(videoURL).openStream();
                videoIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e(TAG, e.getLocalizedMessage());
            }
            return videoIcon;
        }

        public void onPostExecute(Bitmap result) {
            videoImage.setImageBitmap(result);
            videoImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, VideoPlayer.class);
                    intent.putExtra("videoId", videoId);
                    context.startActivity(intent);
                }
            });
        }
    }
}
